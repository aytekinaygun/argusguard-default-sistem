#!/usr/bin/env python
# -*- coding: utf-8 -*-

import MySQLdb
import os
from time import gmtime, strftime # zaman bilgisi icin
import configobj

config = configobj.ConfigObj('/etc/argusguard/argusguard.conf')
hostname = "localhost"
databasename = config['databasename']
databaseuser = config['databaseuser']
databasepass = config['databasepass']
LAN = config['LAN']
INT_IP = config['INT_IP']
EXT_IF = config['EXT_IF']
INT_IF = config['INT_IF']
CENTER = config['center']
contentfilterif = config['contentfilterif']

db = MySQLdb.connect(host=hostname,user=databaseuser,passwd=databasepass,db=databasename)
cursor = db.cursor()

zaman = strftime("%Y-%m-%d-%H-%M-%S")
backup_script_name = "firewall-argusguard-"+zaman+".sh"
backup_dosya = "/var/backup/fw-backup/"+backup_script_name
firewall_argusguard_sh = open(backup_dosya, "w")

################################################################
#############  firewall-argusguard.sh ##########################
################################################################
firewall_top_1 = """#!/bin/bash

# Tarih : %s

# Kurallar silinir
iptables -t filter -F
iptables -t nat -F
iptables -t mangle -F
iptables -t filter -X
iptables -t nat -X
iptables -t mangle -X

# Moduller yuklenir
depmod -a
modprobe ip_tables
modprobe ip_conntrack
modprobe ip_conntrack_ftp
modprobe ip_conntrack_irc
modprobe iptable_nat
modprobe ip_nat_ftp
modprobe ip_nat_irc

echo "1" > /proc/sys/net/ipv4/ip_forward
echo "1" > /proc/sys/net/ipv4/ip_dynaddr

# DDOS ATAK
iptables -N drop-saldiri
iptables -A drop-saldiri -j LOG --log-level warning --log-prefix "DROPED SYN-FLOOD : "
iptables -A drop-saldiri -j DROP
iptables -N syn-flood 
iptables -A syn-flood -m limit --limit=10/s --limit-burst 24 -j RETURN 
iptables -A syn-flood -j drop-saldiri

# LOG KAYIT
iptables -N drop-input
iptables -A drop-input -j LOG --log-level warning --log-prefix "DROPED INPUT : "
iptables -A drop-input -j DROP
iptables -N drop-output
iptables -A drop-output -j LOG --log-level warning --log-prefix "DROPED OUTPUT : "
iptables -A drop-output -j DROP
iptables -N drop-forward
iptables -A drop-forward -j LOG --log-level warning --log-prefix "DROPED FORWARD : "
iptables -A drop-forward -j DROP

# GENEL IZINLER
iptables -A INPUT -i lo -j ACCEPT
iptables -A OUTPUT -o lo -j ACCEPT
iptables -A OUTPUT -o eth0 -j ACCEPT
iptables -A FORWARD -m state --state ESTABLISHED,RELATED -j ACCEPT
iptables -A INPUT -m state --state ESTABLISHED,RELATED -j ACCEPT
iptables -A OUTPUT -m state --state NEW,ESTABLISHED,RELATED -j ACCEPT

iptables -A INPUT -i %s -s %s -p tcp --dport 26767 -j ACCEPT
iptables -A INPUT -p tcp --dport 26868 -j ACCEPT

""" % (zaman, INT_IF, LAN)

if EXT_IF.find(",") < 0:
    firewall_top_2 = "iptables -t nat -A POSTROUTING -s %s -o %s -j MASQUERADE" % (LAN, EXT_IF)
else:
    firewall_top_2 = ""
    for i in EXT_IF.split(","):
        firewall_top_2 = firewall_top_2 + "\n" +"iptables -t nat -A POSTROUTING -s %s -o %s -j MASQUERADE" % (LAN, i)

firewall_argusguard_sh.write(firewall_top_1)
firewall_argusguard_sh.write(firewall_top_2)
firewall_argusguard_sh.write("\n\n")

# Merkez sunucu degilse Center'dan basilan kurallar
if CENTER != "True":
	f = open("/usr/local/sbin/fw/fw-center-rules","r")
	rulesfile = f.readlines()
	for line in rulesfile:
		firewall_argusguard_sh.write(line)

# Bu fw'ye ozel komutlar yazilir.
f = open("/usr/local/sbin/fw/fw-local-rules","r")
rulesfile = f.readlines()
for line in rulesfile:
	firewall_argusguard_sh.write(line)

# Host bilgilerini dondurur
def InfoHost(HostID):
	global RedirectHostID
	global HostComment
	global HostAddress
	global HostNetmask
	global HostName
	
	cursor.execute("SELECT * FROM `host` WHERE `id`=%s" % (HostID))
	row = cursor.fetchone()
	HostName = row[3]
	HostAddress = str(row[4])
	HostNetmask = str(row[5])
	HostComment = row[6]

# Hostlar icin Gruba uye olan hostlarin listesini dondurur		
def GroupHostsList(GroupID):
	global HostList
	HostList = []

	cursor.execute("SELECT * FROM `groupitem` WHERE `group`=%s" % (GroupID))
	sonuc = cursor.fetchall()

	for row in sonuc:
	    HostList.append(row[1])
	return HostList

# Servis bilgilerini dondurur
def InfoService (ServiceID):
	global ServiceProtokol
	global ServicePort
	global ServiceComment

	cursor.execute("SELECT * FROM `service` WHERE `id`=%s" % (ServiceID))
	row = cursor.fetchone()
	ServicePort  = str(row[3])
	ServiceProtokol = row[4]
	ServiceComment = row[5]
			
# Local --> internet : Localden internete kurallar yazilir.
def CreateRules_local2internet (Policy_Chain, RuleIface, sHostID, dHostID, ServiceName, RulePolicy):
	
	InfoHost(sHostID)
	sHostName = HostName
	sHostComment = HostComment
	sHostAddress = HostAddress
	sHostNetmask = HostNetmask
	
	InfoHost(dHostID)
	dHostName = HostName
	dHostComment = HostComment
	dHostAddress = HostAddress
	dHostNetmask = HostNetmask

	InfoService(ServiceName)
	
	firewall_argusguard_sh.write("### "+sHostName+" --> "+dHostName+"\n")
	
	# Kural olustur.
	ifrule = " -i "+RuleIface
	sHostRule = " -s "+sHostAddress+"/"+sHostNetmask
	dHostRule = " -d "+dHostAddress+"/"+dHostNetmask
	ServiceRule = " -p "+ServiceProtokol+" --dport "+ServicePort
	
	if RuleIface == "Any":
		ifrule = ""
	
	if ServiceName == 1:
		ServiceRule = ""

	if RulePolicy == "ACCEPT":
		firewall_argusguard_sh.write("iptables -t nat -A PREROUTING"+ifrule+sHostRule+dHostRule+ServiceRule+" -j "+RulePolicy+"\n")
	
	firewall_argusguard_sh.write("iptables -A FORWARD"+ifrule+sHostRule+dHostRule+ServiceRule+" -j "+Policy_Chain+"\n")

# internet --> Local : internetten locale kurallar yazilir.
def CreateRules_internet2local(Policy_Chain, RuleIface, sHostID, dHostID, ServiceID, RedirectHostID, RedirectServiceID, RulePolicy):
	
	InfoHost(sHostID)
	sHostName = HostName
	sHostComment = HostComment
	sHostAddress = HostAddress
	sHostNetmask = HostNetmask
	
	InfoHost(dHostID)
	dHostName = HostName
	dHostComment = HostComment
	dHostAddress = HostAddress
	dHostNetmask = HostNetmask

	InfoHost(RedirectHostID)
	rHostName = HostName
	rHostComment = HostComment
	rHostAddress = HostAddress
	rHostNetmask = HostNetmask

	InfoService(ServiceID)
	dServiceProtokol = ServiceProtokol
	dServicePort = ServicePort

	InfoService(RedirectServiceID)
	rServicePort = ServicePort
	rServiceProtokol = ServiceProtokol

	firewall_argusguard_sh.write("### "+sHostName+" --> "+dHostName+"\n")

	# Kural olustur.
	ifrule = " -i "+RuleIface
	sHostRule = " -s "+sHostAddress+"/"+sHostNetmask
	dHostRule = " -d "+dHostAddress+"/"+dHostNetmask 
	dServiceRule = " -p "+dServiceProtokol+" --dport "+dServicePort
	RedirectRule = " -j DNAT --to "+rHostAddress+":"+rServicePort+"\n"
	SourceNat = " -j SNAT --to "+INT_IP+"\n"

	if RuleIface == "Any":
		ifrule = ""

	if ServiceID == "1":
		dServiceRule = ""

	firewall_argusguard_sh.write("iptables -t nat -A POSTROUTING"+sHostRule+dHostRule+dServiceRule+SourceNat)	
	firewall_argusguard_sh.write("iptables -t nat -A PREROUTING"+ifrule+sHostRule+dServiceRule+RedirectRule)
	firewall_argusguard_sh.write("iptables -A FORWARD"+ifrule+\
									" -d "+rHostAddress+\
									" -p "+rServiceProtokol+" --dport "+rServicePort+\
									" -j "+Policy_Chain+"\n")

# Local --> Firewall <-- internet : Kurallar yazilir.
def CreateRules_InputFirewall(Policy_Chain, RuleIface, sHostID, ServiceID, RulePolicy):
	
	InfoHost(sHostID)
	sHostName = HostName
	sHostComment = HostComment
	sHostAddress = HostAddress
	sHostNetmask = HostNetmask

	InfoService(ServiceID)
	
	firewall_argusguard_sh.write("### "+sHostName+" --> Firewall\n")
	
	# Kural olustur.
	ifrule = " -i "+RuleIface
	sHostRule = " -s "+sHostAddress+"/"+sHostNetmask
	ServiceRule = " -p "+ServiceProtokol+" --dport "+ServicePort

	if RuleIface == "Any":
		ifrule = ""

	if ServiceID == "1":
		ServiceRule = ""

	firewall_argusguard_sh.write("iptables -A INPUT"+ifrule+sHostRule+ServiceRule+" -j "+Policy_Chain+"\n")

#########################################################
################## Kurallar islenir #####################
#########################################################


# Local --> internet : Localden internete kurallar islenir.
firewall_argusguard_sh.write("######################################\n")
firewall_argusguard_sh.write("####  Local --> internet  ############\n")

cursor.execute("SELECT * FROM `lantointernet`")
sonuc = cursor.fetchall()
for row in sonuc:
	RuleNo = row[0]
	RuleNoName = "Lan-To-Internet-"+str(RuleNo)
	sHostID = row[3]
	dHostID = row[5]
	ServiceName = row[6]
	Policy_Chain = row[7] + "_" + RuleNoName
	RuleIface = row[1]
	RuleComment = row[8]
	RulePolicy = row[7]
	
	# Zincir olustur.
	firewall_argusguard_sh.write("######################################\n")
	firewall_argusguard_sh.write("iptables -N "+Policy_Chain+"\n")
	firewall_argusguard_sh.write("iptables -A "+Policy_Chain+" -j LOG --log-level warning --log-prefix \""+Policy_Chain+" : \" \n")
	firewall_argusguard_sh.write("iptables -A "+Policy_Chain+" -j "+RulePolicy+"\n")

	# Eger kaynak ve hedef host ise (ikiside grup degil)
	if row[2] == 0 and row[4] == 0:
		CreateRules_local2internet(Policy_Chain, RuleIface, sHostID, dHostID, ServiceName, RulePolicy)
	# Eger sadece kaynak, grup ise
	if row[2] == 1 and row[4] == 0:
		sGroupID = row[3]
		sHstLst = GroupHostsList(sGroupID)
		for sHostID in sHstLst:
			CreateRules_local2internet(Policy_Chain, RuleIface, sHostID, dHostID, ServiceName, RulePolicy)
	# Eger sadece Hedef, grup ise
	if row[2] == 0 and row[4] == 1:
		dGroupID = row[5]
		dHstLst = GroupHostsList(dGroupID)
		for dHostID in dHstLst:
			CreateRules_local2internet(Policy_Chain, RuleIface, sHostID, dHostID, ServiceName, RulePolicy)
	# Eger kaynak ve hedef grup ise
	if row[2] == 1 and row[4] == 1:
		sGroupID = row[3]
		dGroupID = row[5]
		sHstLst = GroupHostsList(sGroupID)
		dHstLst = GroupHostsList(dGroupID)
		for sHostID in sHstLst:
			for dHostID in dHstLst:
				CreateRules_local2internet(Policy_Chain, RuleIface, sHostID, dHostID, ServiceName, RulePolicy)
	firewall_argusguard_sh.write("\n")

# internet --> Local: internetten locale kurallar islenir.
firewall_argusguard_sh.write("######################################\n")
firewall_argusguard_sh.write("####  internet --> Local  ############\n")
cursor.execute("SELECT * FROM `internettolan`")
sonuc = cursor.fetchall()
for row in sonuc:
	RuleNo = row[0]
	RuleNoName = "Internet-To-Local-"+str(RuleNo)
	sHostID = row[3]
	dHostID = row[4]
	ServiceName = row[5]
	Policy_Chain = row[8] + "_" + RuleNoName
	RuleIface = row[1]
	RuleComment = row[9]
	RulePolicy = row[8]
	RedirectHostID = row[6]
	RedirectServiceID = row[7]
	
	# Zincir olustur.
	firewall_argusguard_sh.write("######################################\n")
	firewall_argusguard_sh.write("iptables -N "+Policy_Chain+"\n")
	firewall_argusguard_sh.write("iptables -A "+Policy_Chain+" -j LOG --log-level warning --log-prefix \""+Policy_Chain+" : \" \n")
	firewall_argusguard_sh.write("iptables -A "+Policy_Chain+" -j "+RulePolicy+"\n")

	# Eger kaynak host ise (grup degil)
	if row[2] == 0:
		CreateRules_internet2local(Policy_Chain, RuleIface, sHostID, dHostID, ServiceName, RedirectHostID, RedirectServiceID, RulePolicy)
	# Eger kaynak, grup ise
	if row[2] == 1:
		sGroupID = row[3]
		sHstLst = GroupHostsList(sGroupID)
		for sHostID in sHstLst:
			CreateRules_internet2local(Policy_Chain, RuleIface, sHostID, dHostID, ServiceName, RedirectHostID, RedirectServiceID, RulePolicy)
	firewall_argusguard_sh.write("\n")

# Local --> Firewall <-- internet : internet ve localden firewall'a kurallar islenir.
firewall_argusguard_sh.write("##############################################\n")
firewall_argusguard_sh.write("####  Local --> Firewall <-- internet  #######\n")
cursor.execute("SELECT * FROM `inputfirewall`")
sonuc = cursor.fetchall()
for row in sonuc:
	RuleNo = row[0]
	RuleNoName = "Input-Firewall-"+str(RuleNo)
	RuleIface = row[1]
	sHostID = row[3]
	ServiceID = row[4]
	RulePolicy = row[5]
	RuleComment = row[6]
	Policy_Chain = row[5] + "_" + RuleNoName
	
	# Zincir olustur.
	firewall_argusguard_sh.write("######################################\n")
	firewall_argusguard_sh.write("iptables -N "+Policy_Chain+"\n")
	firewall_argusguard_sh.write("iptables -A "+Policy_Chain+" -j LOG --log-level warning --log-prefix \""+Policy_Chain+" : \" \n")
	firewall_argusguard_sh.write("iptables -A "+Policy_Chain+" -j "+RulePolicy+"\n")

	# Eger kaynak host ise (grup degil)
	if row[2] == 0:
		CreateRules_InputFirewall(Policy_Chain, RuleIface, sHostID, ServiceID, RulePolicy)
	# Eger kaynak, grup ise
	if row[2] == 1:
		sGroupID = row[3]
		sHstLst = GroupHostsList(sGroupID)
		for sHostID in sHstLst:
			CreateRules_InputFirewall(Policy_Chain, RuleIface, sHostID, ServiceID, RulePolicy)
	firewall_argusguard_sh.write("\n")

# dansguardian'in dinledigi arayuz ayari.
if contentfilterif.find(",") < 0:
    firewall_argusguard_sh.write("iptables -t nat -A PREROUTING -i %s -p tcp --dport 80 -j REDIRECT --to 8080\n" % (contentfilterif)) 
else:
    #firewall_top_2 = ""
    for i in contentfilterif.split(","):
        firewall_argusguard_sh.write("iptables -t nat -A PREROUTING -i %s -p tcp --dport 80 -j REDIRECT --to 8080\n" % (i))

firewall_argusguard_sh.write("iptables -A INPUT -j drop-input\n")
firewall_argusguard_sh.write("iptables -A FORWARD -j drop-forward\n")
firewall_argusguard_sh.write("iptables -A OUTPUT -j drop-output\n")

firewall_argusguard_sh.close()
cursor.close ()
db.close ()

os.system("cp " + backup_dosya + " /usr/local/sbin/firewall-argusguard.sh")
os.system("chmod +x /usr/local/sbin/firewall-argusguard.sh")