#!/usr/bin/env python
# -*- coding: utf-8 -*-

import MySQLdb
import configobj
import os
import datetime
import time

config = configobj.ConfigObj('/etc/argusguard/argusguard.conf')
hostname = "localhost"
databasename = config['databasename']
databaseuser = config['databaseuser']
databasepass = config['databasepass']

db = MySQLdb.connect(host=hostname,user=databaseuser,passwd=databasepass,db=databasename)
cursor = db.cursor()

logpath = "/var/log/dansguardian/"

dirs = os.listdir(logpath)
newlist = []
for names in dirs:
    if names.startswith("access.log."):
        newlist.append(names)

for i in newlist:
    
	f = open(logpath+i)
	logfile = f.readlines()

	for line in logfile:
		# Satirlar "," ile ayrilir. Liste olusturulur.
		linelist = line.split("\",\"")
		Date_Time = linelist[0].split(" ")
		Date = Date_Time[0].replace("\"", "")
		Time = Date_Time[1]
		User = linelist[1]
		IP = linelist[2]
		URL  = linelist[3].replace("'", "")
		Domain  = linelist[3].split("/")[2]
		Action  = linelist[4]
		Method = linelist[5]
		Size = linelist[6]
		weight = linelist[7]
		category = linelist[8]
		groupnumber = linelist[9]
		HTTP_Code = linelist[10]
		mimetype = linelist[11]
		clientname = linelist[12]
		groupname = linelist[13]
		user_agent = linelist[14]

		# bing.com gibi parse ederken sorun olan satirlari gormezden gelmek icin 
		# 15 alani olanlari suzuyoruz.
		if len(linelist) == 15:
			sorgu = """
			insert into alllogs 
			(Date, Time, User, IP, URL, Domain, Action, Method, Size, weight, category, groupnumber, HTTP_Code, mimetype, clientname, groupname, user_agent) 
			values 
			('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s')""" \
			% \
			(Date, Time, User, IP, URL, Domain, Action, Method, Size, weight, category, groupnumber, HTTP_Code, mimetype, clientname, groupname, user_agent)
			cursor.execute(sorgu)
		else:
			print URL 

	f.close

	# Dosyaların sıkıştırılması ve taşınması
	# Zaman bilgisi YYYY-AA-GG-SS-DD-SS-mikrosaniye şeklinde oluşturulur.
	today = datetime.datetime.now()
	zaman = str(today.year)+"-"+str(today.month).zfill(2)+"-"+str(today.day).zfill(2)+"-"+ \
	       str(today.hour).zfill(2)+"-"+str(today.minute).zfill(2)+"-"+str(today.second).zfill(2)+"-"+str(today.microsecond).zfill(2)
	
	compressfilename = i + "-" + zaman + ".tar.gz"
	
	os.system("/usr/local/sbin/backup_dg_logs.sh %s %s %s" % (logpath, i, compressfilename))
	

db.commit()
cursor.close ()
db.close ()