#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
import time
from time import strftime

LOG_PATH = "/var/log/argusguard/"
IMZA_PATH = "/var/log/argusguard/imza/"
DHCP_LOG = "/var/lib/dhcp/dhcpd.leases"

zaman = strftime("%Y-%m-%d-%H-%M-%S")

# dhcp log dosyasi var ise
if os.path.exists(DHCP_LOG):
	os.system("cp %s %sdhcpd.leases.%s;gzip %sdhcpd.leases.%s" % (DHCP_LOG, LOG_PATH, zaman, LOG_PATH, zaman))

LOG_FILES = os.listdir(LOG_PATH)

for i in LOG_FILES:
	if i.endswith("gz"): # gz ile bitenler
		os.system("openssl  ts  -query  -data  /var/log/argusguard/%s  -no_nonce  -out  /var/log/argusguard/%s.tsq" % (i, i))
		os.system("openssl  ts  -reply  -queryfile  /var/log/argusguard/%s.tsq  -out  /var/log/argusguard/%s.der  -token_out -config /etc/ssl/imza-openssl.cnf -passin pass:12345678" % (i, i))
		os.system("mv /var/log/argusguard/%s* /var/log/argusguard/imza/" % (i))
