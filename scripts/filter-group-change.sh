#!/bin/bash

filterpath="/etc/argusguard/dansguardian/lists"
confpath="/etc/argusguard/dansguardian"

mv $filterpath/filtergroups$1 $filterpath/filtergroupsX
mv $filterpath/filtergroups$2 $filterpath/filtergroups$1
mv $filterpath/filtergroupsX $filterpath/filtergroups$2

sed -i s/filtergroups$2/filtergroups$1/g $filterpath/filtergroups$1/exceptionsitelist
sed -i s/filtergroups$1/filtergroups$2/g $filterpath/filtergroups$2/exceptionsitelist

sed -i s/filtergroups$2/filtergroups$1/g $filterpath/filtergroups$1/exceptionurllist
sed -i s/filtergroups$1/filtergroups$2/g $filterpath/filtergroups$2/exceptionurllist

sed -i s/filtergroups$2/filtergroups$1/g $filterpath/filtergroups$1/bannedsitelist
sed -i s/filtergroups$1/filtergroups$2/g $filterpath/filtergroups$2/bannedsitelist

sed -i s/filtergroups$2/filtergroups$1/g $filterpath/filtergroups$1/bannedurllist
sed -i s/filtergroups$1/filtergroups$2/g $filterpath/filtergroups$2/bannedurllist


mv $confpath/dansguardianf$1.conf $confpath/dansguardianfX.conf 
mv $confpath/dansguardianf$2.conf $confpath/dansguardianf$1.conf
mv $confpath/dansguardianfX.conf $confpath/dansguardianf$2.conf

sed -i s/filtergroups$2/filtergroups$1/g $confpath/dansguardianf$1.conf
sed -i s/filtergroups$1/filtergroups$2/g $confpath/dansguardianf$2.conf

chown -R www-data: /etc/argusguard/dansguardian

