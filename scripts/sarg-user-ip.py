#!/usr/bin/env python
#-*- coding: utf-8 -*- 

import MySQLdb
import configobj
import os

config = configobj.ConfigObj('/etc/argusguard/argusguard.conf')
hostname = "localhost"
databasename = config['databasename']
databaseuser = config['databaseuser']
databasepass = config['databasepass']

usertab = open("/etc/sarg/usertab", "w")

db = MySQLdb.connect(host=hostname,user=databaseuser,passwd=databasepass,db=databasename)
cursor = db.cursor()
cursor.execute("SET NAMES 'utf8'")

cursor.execute("SELECT * FROM host a left join filtre_kullanicilar b on b.kullanici=a.id where a.netmask='255.255.255.255'")
sonuc = cursor.fetchall()

for row in sonuc:
	
	if str(row[10]) == 'None':
		group_name = "Default"
	else:
		conffile = configobj.ConfigObj('/etc/dansguardian/dansguardianf'+str(row[10])+'.conf')
		group_name =conffile['groupname']
	
	username_ip = row[4]+" "+row[3]+"["+group_name+"]"+"\n"
	usertab.write(username_ip)

usertab.close ()
cursor.close ()
db.close ()