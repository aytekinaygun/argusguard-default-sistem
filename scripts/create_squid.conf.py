#!/usr/bin/env python
# -*- coding: utf-8 -*-

def not_subdomain(bende_bilemedim_ama_gerekli):
	# Bu fonksiyon subdomainleri ve aynı kayıtları siler.

	site_listesi_silinecekler = []

	# i nci satirdaki site ile satirdan sonraki sitelerin karşılaştırması.
	for i in range(0,len(site_listesi)-1): # liste ogeleri sondan birinciye kadar dondurulur. son oge hata vereceginden dondurulmez.

		for j in range(i+1, len(site_listesi)):
			domain = site_listesi[i].strip() # sağ ve sol boslukları temizle
			subdomain = site_listesi[j].strip()
			if subdomain.endswith("." + domain) or domain == subdomain: # "a.b.com.tr", ".b.com.tr" ile bitiyor ise. yada aynı ise
				# aynı öğeden iki tane olmaması için. Bunlar silineceklerin index sayıları ve silme işleminde  hata olmaması için aynı
				# öğeden 2 tane olmamalı. 
				if site_listesi_silinecekler.count(j) == 0: 
					site_listesi_silinecekler.append(j) # silineceklerin indexleri liste olarak toplanır.
					
	# önce sırala sonra tersine çevir. 
	# Listeden silme işlemi büyük indexden başlayıp küçüğe doğru olsun ki silme işlemi hata vermesin.
	# küçükte büyüğe olur ise index no'da kayma olur.			
	site_listesi_silinecekler.sort() 
	site_listesi_silinecekler.reverse() 

	# silme işlemi.
	for x in site_listesi_silinecekler:
		site_listesi.pop(x)


# filtre gruplarına ait kullanıcıların ip adresleri ayiklanir
# ilgili dosyaya yazilir.
f = open("/etc/dansguardian/lists/authplugins/ipgroups","r")
users = f.readlines()

filtre_grubu_listesi = []
for line in users:
	filtre_grubu_adi = line.split('=')[1].strip() # strip tab ve bosluk siliyor.
	sayi = filtre_grubu_adi.split("filter")[1]
	filtre_grubu_listesi.append(sayi)

maxfilternumber = int(max(filtre_grubu_listesi))+1

for i in range(2, maxfilternumber):
	filterx = "filter"+str(i)
	squid_file = open("/etc/squid3/rules/"+filterx, "w")
	for line in users:		
		if line.find(filterx) > 0:
			squid_file.write(line.split("/")[0]+"\n")

# Tum kullanicilara izinli sitelerin listesi:
f = open("/etc/dansguardian/lists/exceptionsitelist-all","r")
site_listesi = f.readlines()

not_subdomain(site_listesi)
not_subdomain(site_listesi.reverse())

squid_file = open("/etc/squid3/rules/exceptionsitelist-all", "w")
squid_file_kumesi = set()
for line in site_listesi:
	if line.find(".") > 0:
		squid_file_kumesi.add("."+line.strip()+"\n")

for i in squid_file_kumesi:
	squid_file.write(i)

# Tum kullanicilara yasaklı sitelerin listesi:
f = open("/etc/dansguardian/lists/bannedsitelist-all","r")
site_listesi = f.readlines()

not_subdomain(site_listesi)
not_subdomain(site_listesi.reverse())

squid_file = open("/etc/squid3/rules/bannedsitelist-all", "w")
squid_file_kumesi = set()
for line in site_listesi:
	if line.find(".") > 0 and line.startswith("#") == False:
		squid_file.write("."+line.strip()+"\n")

for i in squid_file_kumesi:
	squid_file.write(i)

list_filename = ["exceptionsitelist-fg", "bannedsitelist-fg"]
for filename in list_filename:
	# Grupların dosyaları oluşturulur.
	for i in range(2, maxfilternumber):
		f = open("/etc/dansguardian/lists/filtergroups"+str(i)+"/"+filename,"r")
		site_listesi = f.readlines()

		not_subdomain(site_listesi)
		not_subdomain(site_listesi.reverse())

		squid_file = open("/etc/squid3/rules/filter"+str(i)+"-"+filename, "w")
		squid_file_kumesi = set()
		for line in site_listesi:
			if line.find(".") > 0 and line.startswith("#") == False:
				squid_file_kumesi.add("."+line.strip()+"\n")

		for i in squid_file_kumesi:
			squid_file.write(i)