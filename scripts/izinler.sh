#!/bin/bash
chown -R www-data: /var/www/argusguard
chown -R www-data: /etc/argusguard/dansguardian
chown -R root: /etc/dansguardian
chown -R www-data: /var/backup/fw-backup
chmod +x /var/backup/fw-backup/*