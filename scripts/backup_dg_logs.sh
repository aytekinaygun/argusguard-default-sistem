#/bin/bash

# $1 : Dansguardian Log files path
# $2 : Log file name
# $3 : Compress log file name
#

cd $1
tar zcf archive/$3 $2

# Log dosyasi silinir.
rm $1$2