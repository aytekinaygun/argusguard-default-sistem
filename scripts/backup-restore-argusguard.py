#!/usr/bin/env python
# -*- coding: utf-8 -*-

# backup ve restore olarak iki parametre alır.
# Eğer restore parametresi verilecek ise /var/backup/ dizini altındaki
# tarih biçimli dizin isimlerinden biri de parametre olarak verilmelidir.
# Sistem verilecek tarihli dizin içindeki yapıya geri döner.

import MySQLdb
import os
from time import gmtime, strftime # zaman bilgisi icin
import configobj
import sys

config = configobj.ConfigObj('/etc/argusguard/argusguard.conf')
hostname = "localhost"
databasename = config['databasename']
databaseuser = config['databaseuser']
databasepass = config['databasepass']
if_backup_restore = sys.argv[1]

db = MySQLdb.connect(host=hostname,user=databaseuser,passwd=databasepass,db=databasename)
cursor = db.cursor()

zaman = strftime("%Y-%m-%d-%H-%M-%S")
backup_path = "/var/backup/"+zaman

if if_backup_restore == "backup":
	os.system("mkdir "+backup_path)
	# mysql yedek.
	mysql_backup_file = backup_path+"/mysqlbackup-"+zaman+".gz"

	os.system("mysqldump -u "+databaseuser+" -p"+databasepass+" "+databasename+" \
		      --ignore-table=argusguard.alllogs --ignore-table=argusguard.gunluk_rapor_bilgi|gzip > "+mysql_backup_file)

	# dansguardian yedek.
	os.system("rsync -az --exclude \"blacklists\" /etc/argusguard/dansguardian "+backup_path)

	# fw ilişkili dosyalar yedek.
	os.system("cp -r /usr/local/sbin/fw "+backup_path)

	# conf yedek.
	os.system("cp /etc/argusguard/argusguard.conf "+backup_path)


if if_backup_restore == "restore":
	
	restore_date = sys.argv[2]
	
	# alllogs ve gunluk_rapor_bilgi tabloları hariç, tüm tablolar silinir.
	tablo_sil = """SET @tables = NULL;
	SELECT GROUP_CONCAT(table_schema, '.', table_name) INTO @tables FROM information_schema.tables 
	  WHERE table_schema = 'argusguard' AND table_name <> 'alllogs' AND table_name <> 'gunluk_rapor_bilgi';

	SET @tables = CONCAT('DROP TABLE ', @tables);
	PREPARE stmt1 FROM @tables;
	EXECUTE stmt1;
	DEALLOCATE PREPARE stmt1;"""
	cursor.execute(tablo_sil)

	os.system("gunzip -c /var/backup/"+restore_date+"/mysqlbackup-"+restore_date+".gz > /tmp/mysqlbackup-"+restore_date)
	os.system("mysql -u %s -p%s argusguard < /tmp/mysqlbackup-" % (databaseuser, databasepass) + restore_date)
	os.system("rm /tmp/mysqlbackup-"+restore_date)

	# dansguardian 
	os.system("rsync -az --exclude \"blacklists\" --delete "+"/var/backup/"+restore_date+"/dansguardian /etc/argusguard")

	# fw ilişkili dosyalar yedek.
	os.system("cp -r /var/backup/"+restore_date+"/fw /usr/local/sbin")

	# conf 
	os.system("cp /var/backup/"+restore_date+"/argusguard.conf /etc/argusguard")

	# restart
	os.system("sudo /usr/local/sbin/xml2iptables.py&&sudo /usr/local/sbin/firewall-argusguard.sh")
	os.system("sudo /usr/local/sbin/dansguardian_restart.sh")