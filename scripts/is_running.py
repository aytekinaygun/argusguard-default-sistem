#!/usr/bin/env python
# -*- coding: utf-8 -*-

import re
import subprocess
import sys

# True degeri donmemesi icin parametre olarak buyuk harf gonderilmelidir.
# Gonderilen parametre kucuk harfe cevrilir. 
service_name = sys.argv[1].lower()

def is_running(process):
        s = subprocess.Popen(["ps", "axw"],stdout=subprocess.PIPE)
        for x in s.stdout:
                if re.search(process, x):
                        return True
        return False

if is_running(service_name):
        print "T"
else:
        print "F"

