#!/usr/bin/env python
# -*- coding: utf-8 -*-
import netifaces

dosya = open("/var/www/argusguard/resource/info_if", "w")

interfaces = netifaces.interfaces()
for i in interfaces:
    if i == 'lo':
        continue
    iface = netifaces.ifaddresses(i).get(netifaces.AF_INET)
    mac = netifaces.ifaddresses(i).get(netifaces.AF_LINK)
    if iface != None:
        for j in iface:
        	row = i+" "+j['addr']+" "+j['netmask']+" "+"\n"
        	dosya.write(row)

dosya.close()