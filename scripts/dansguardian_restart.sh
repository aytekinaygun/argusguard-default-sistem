#!/bin/bash

rsync -az /etc/argusguard/dansguardian /etc
chown root.root -R /etc/dansguardian
/etc/init.d/dansguardian reload
