-- phpMyAdmin SQL Dump
-- version 3.4.11.1deb2+deb7u1
-- http://www.phpmyadmin.net
--
-- Anamakine: localhost
-- Üretim Zamanı: 07 Kas 2014, 18:43:57
-- Sunucu sürümü: 5.5.40
-- PHP Sürümü: 5.4.34-0+deb7u1

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Veritabanı: `argusguard`
--

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `alllogs`
--

CREATE TABLE IF NOT EXISTS `alllogs` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Date` date NOT NULL,
  `Time` time NOT NULL,
  `User` varchar(30) NOT NULL,
  `IP` varchar(20) NOT NULL,
  `URL` text NOT NULL,
  `Domain` text NOT NULL,
  `Action` text NOT NULL,
  `Method` varchar(50) NOT NULL,
  `Size` int(50) NOT NULL,
  `weight` int(11) DEFAULT NULL,
  `category` varchar(50) DEFAULT NULL,
  `groupnumber` int(11) DEFAULT NULL,
  `HTTP_Code` int(11) DEFAULT NULL,
  `mimetype` varchar(50) DEFAULT NULL,
  `clientname` varchar(50) DEFAULT NULL,
  `groupname` varchar(80) DEFAULT NULL,
  `user_agent` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ID` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `filtre_kategori`
--

CREATE TABLE IF NOT EXISTS `filtre_kategori` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `durum` tinyint(4) NOT NULL DEFAULT '0',
  `kategori` varchar(45) NOT NULL,
  `izinli_yasakli` tinyint(4) NOT NULL,
  `domain` varchar(150) NOT NULL,
  `url` varchar(150) NOT NULL,
  `category` varchar(45) DEFAULT NULL,
  `aciklama` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2165 ;

--
-- Tablo döküm verisi `filtre_kategori`
--

INSERT INTO `filtre_kategori` (`id`, `durum`, `kategori`, `izinli_yasakli`, `domain`, `url`, `category`, `aciklama`) VALUES
(2085, 1, 'kürtaj ', 1, '.Include</etc/dansguardian/lists/blacklists/abortion/domains>', '.Include</etc/dansguardian/lists/blacklists/abortion/urls>', 'abortion', 'Kürtaj ile ilgili bilgiler '),
(2086, 1, 'reklamlar ', 2, '.Include</etc/dansguardian/lists/blacklists/ads/domains>', '.Include</etc/dansguardian/lists/blacklists/ads/urls>', 'ads', 'Reklam sunucuları ve yasaklı URL''ler '),
(2087, 1, 'yetişkin ', 2, '.Include</etc/dansguardian/lists/blacklists/adult/domains>', '.Include</etc/dansguardian/lists/blacklists/adult/urls>', 'adult', 'Yetişkinler için siteler (pornografik değil) '),
(2088, 1, 'agresif ', 1, '.Include</etc/dansguardian/lists/blacklists/aggressive/domains>', '.Include</etc/dansguardian/lists/blacklists/aggressive/urls>', 'aggressive', 'Şiddete teşvik eden siteler'),
(2089, 1, 'alkol', 2, '.Include</etc/dansguardian/lists/blacklists/alcohol/domains>', '.Include</etc/dansguardian/lists/blacklists/alcohol/urls>', 'alcohol', 'Alkol ile ilgili siteler'),
(2090, 1, 'antispyware ', 1, '.Include</etc/dansguardian/lists/blacklists/antispyware/domains>', '.Include</etc/dansguardian/lists/blacklists/antispyware/urls>', 'antispyware', 'Casus yazılımlardan korunma siteleri'),
(2091, 1, 'artnudes ', 2, '.Include</etc/dansguardian/lists/blacklists/artnudes/domains>', '.Include</etc/dansguardian/lists/blacklists/artnudes/urls>', 'artnudes', 'Sanatsal çıplaklık içeren sanat siteleri '),
(2092, 0, 'astroloji ', 2, '.Include</etc/dansguardian/lists/blacklists/astrology/domains>', '.Include</etc/dansguardian/lists/blacklists/astrology/urls>', 'astrology', 'Astroloji web siteleri '),
(2093, 1, 'audio-video', 1, '.Include</etc/dansguardian/lists/blacklists/audio-video/domains>', '.Include</etc/dansguardian/lists/blacklists/audio-video/urls>', 'audio-video', 'Müzik ve video siteleri'),
(2094, 0, 'bankalar', 1, '.Include</etc/dansguardian/lists/blacklists/bank/domains>', '.Include</etc/dansguardian/lists/blacklists/bank/urls>', 'bank', 'Bankalar'),
(2095, 0, 'bankacılık ', 1, '.Include</etc/dansguardian/lists/blacklists/banking/domains>', '.Include</etc/dansguardian/lists/blacklists/banking/urls>', 'banking', 'Bankacılık web siteleri '),
(2096, 0, 'beerliquorinfo ', 1, '.Include</etc/dansguardian/lists/blacklists/beerliquorinfo/domains>', '.Include</etc/dansguardian/lists/blacklists/beerliquorinfo/urls>', 'beerliquorinfo', 'Sadece bira ya da likör hakkında bilgi içeren siteler'),
(2097, 0, 'beerliquorsale ', 1, '.Include</etc/dansguardian/lists/blacklists/beerliquorsale/domains>', '.Include</etc/dansguardian/lists/blacklists/beerliquorsale/urls>', 'beerliquorsale', 'Bira veya likör satışı yapan siteler'),
(2098, 1, 'blog ', 2, '.Include</etc/dansguardian/lists/blacklists/blog/domains>', '.Include</etc/dansguardian/lists/blacklists/blog/urls>', 'blog', 'Blog web siteleri '),
(2099, 0, 'kitaplar', 1, '.Include</etc/dansguardian/lists/blacklists/books/domains>', '.Include</etc/dansguardian/lists/blacklists/books/urls>', 'books', 'Kitaplarla ilgili siteler'),
(2100, 0, 'ünlüler', 2, '.Include</etc/dansguardian/lists/blacklists/celebrity/domains>', '.Include</etc/dansguardian/lists/blacklists/celebrity/urls>', 'celebrity', 'Ünlülerle ilgili siteler'),
(2101, 1, 'cep telefonu ', 1, '.Include</etc/dansguardian/lists/blacklists/cellphones/domains>', '.Include</etc/dansguardian/lists/blacklists/cellphones/urls>', 'cellphones', 'mobil / cep telefonları için malzeme siteleri'),
(2102, 0, 'sohbet ', 0, '.Include</etc/dansguardian/lists/blacklists/chat/domains>', '.Include</etc/dansguardian/lists/blacklists/chat/urls>', 'chat', 'Sohbet odaları (chat) siteleri'),
(2103, 1, 'çocuk', 2, '.Include</etc/dansguardian/lists/blacklists/child/domains>', '.Include</etc/dansguardian/lists/blacklists/child/urls>', 'child', 'Çocuk siteleri'),
(2104, 0, 'çocuk bakımı ', 0, '.Include</etc/dansguardian/lists/blacklists/childcare/domains>', '.Include</etc/dansguardian/lists/blacklists/childcare/urls>', 'childcare', 'Çocuk bakımı siteleri '),
(2105, 1, 'temizlik ', 0, '.Include</etc/dansguardian/lists/blacklists/cleaning/domains>', '.Include</etc/dansguardian/lists/blacklists/cleaning/urls>', 'cleaning', 'Temizlik ile ilgili siteler '),
(2106, 0, 'giyim ', 0, '.Include</etc/dansguardian/lists/blacklists/clothing/domains>', '.Include</etc/dansguardian/lists/blacklists/clothing/urls>', 'clothing', 'Giyim ile ilgili siteler (alışveriş) '),
(2107, 1, 'kontrasepsiyon ', 0, '.Include</etc/dansguardian/lists/blacklists/contraception/domains>', '.Include</etc/dansguardian/lists/blacklists/contraception/urls>', 'contraception', 'Doğum kontrolü ile ilgili sağlık siteleri'),
(2108, 0, 'Yemek pişirme', 0, '.Include</etc/dansguardian/lists/blacklists/cooking/domains>', '.Include</etc/dansguardian/lists/blacklists/cooking/urls>', 'cooking', 'Yemek pişirme ile ilgili siteler'),
(2109, 1, 'escort ', 0, '.Include</etc/dansguardian/lists/blacklists/dating/domains>', '.Include</etc/dansguardian/lists/blacklists/dating/urls>', 'dating', 'Arkadaşlık siteleri'),
(2110, 0, 'desktopsillies ', 0, '.Include</etc/dansguardian/lists/blacklists/desktopsillies/domains>', '.Include</etc/dansguardian/lists/blacklists/desktopsillies/urls>', 'desktopsillies', 'Ekran koruyucular, arka planlar, masaüstü temaları ve benzeri araçlarla ilgili tehlikeli içerik barındıran siteler '),
(2111, 0, 'dialer ', 2, '.Include</etc/dansguardian/lists/blacklists/dialers/domains>', '.Include</etc/dansguardian/lists/blacklists/dialers/urls>', 'dialers', 'Pornografi için trojan içeren yazılım siteleri '),
(2112, 0, 'ilaçlar ', 1, '.Include</etc/dansguardian/lists/blacklists/drugs/domains>', '.Include</etc/dansguardian/lists/blacklists/drugs/urls>', 'drugs', 'İlaçlar ile ilgili siteler'),
(2113, 0, 'e-ticaret ', 0, '.Include</etc/dansguardian/lists/blacklists/ecommerce/domains>', '.Include</etc/dansguardian/lists/blacklists/ecommerce/urls>', 'ecommerce', 'Online alışveriş siteleri'),
(2114, 0, 'eğlence ', 0, '.Include</etc/dansguardian/lists/blacklists/entertainment/domains>', '.Include</etc/dansguardian/lists/blacklists/entertainment/urls>', 'entertainment', 'Filmler, kitaplar, dergi, mizah siteleri '),
(2115, 0, 'filehosting ', 0, '.Include</etc/dansguardian/lists/blacklists/filehosting/domains>', '.Include</etc/dansguardian/lists/blacklists/filehosting/urls>', 'filehosting', 'Dosya indirme/paylaşım siteleri'),
(2116, 0, 'frencheducation ', 1, '.Include</etc/dansguardian/lists/blacklists/frencheducation/domains>', '.Include</etc/dansguardian/lists/blacklists/frencheducation/urls>', 'frencheducation', 'Fransızca eğitimi siteleri'),
(2117, 1, 'kumar ', 0, '.Include</etc/dansguardian/lists/blacklists/gambling/domains>', '.Include</etc/dansguardian/lists/blacklists/gambling/urls>', 'gambling', 'Hisse senedi, kumar siteleri '),
(2118, 0, 'oyunlar ', 0, '.Include</etc/dansguardian/lists/blacklists/games/domains>', '.Include</etc/dansguardian/lists/blacklists/games/urls>', 'games', 'Oyun ile ilgili siteler '),
(2119, 0, 'bahçe ', 1, '.Include</etc/dansguardian/lists/blacklists/gardening/domains>', '.Include</etc/dansguardian/lists/blacklists/gardening/urls>', 'gardening', 'Bahçe bakımı ile ilgili siteler'),
(2120, 0, 'hükümet ', 0, '.Include</etc/dansguardian/lists/blacklists/government/domains>', '.Include</etc/dansguardian/lists/blacklists/government/urls>', 'government', 'Devlet siteleri'),
(2121, 1, 'silahlar ', 0, '.Include</etc/dansguardian/lists/blacklists/guns/domains>', '.Include</etc/dansguardian/lists/blacklists/guns/urls>', 'guns', 'Silahlarla ilgili siteler '),
(2122, 0, 'hack ', 0, '.Include</etc/dansguardian/lists/blacklists/hacking/domains>', '.Include</etc/dansguardian/lists/blacklists/hacking/urls>', 'hacking', 'Hacking ile ilgili siteler'),
(2123, 0, 'homerepair ', 0, '.Include</etc/dansguardian/lists/blacklists/homerepair/domains>', '.Include</etc/dansguardian/lists/blacklists/homerepair/urls>', 'homerepair', 'Ev tamiri ile ilgili siteler '),
(2124, 0, 'hijyen ', 0, '.Include</etc/dansguardian/lists/blacklists/hygiene/domains>', '.Include</etc/dansguardian/lists/blacklists/hygiene/urls>', 'hygiene', 'Hijyen ve diğer kişisel bakım ile ilgili siteler '),
(2125, 0, 'ınstantmessaging ', 0, '.Include</etc/dansguardian/lists/blacklists/instantmessaging/domains>', '.Include</etc/dansguardian/lists/blacklists/instantmessaging/urls>', 'instantmessaging', 'Mesajlaşma istemcisi indirme ve web tabanlı mesajlaşma siteleri'),
(2126, 0, 'takı ', 0, '.Include</etc/dansguardian/lists/blacklists/jewelry/domains>', '.Include</etc/dansguardian/lists/blacklists/jewelry/urls>', 'jewelry', 'Takı ile ilgili siteler'),
(2127, 0, 'jobsearch ', 0, '.Include</etc/dansguardian/lists/blacklists/jobsearch/domains>', '.Include</etc/dansguardian/lists/blacklists/jobsearch/urls>', 'jobsearch', 'İş bulmak için siteler '),
(2128, 0, 'kidstimewasting ', 0, '.Include</etc/dansguardian/lists/blacklists/kidstimewasting/domains>', '.Include</etc/dansguardian/lists/blacklists/kidstimewasting/urls>', 'kidstimewasting', 'Çocuklar için zaman kaybettirecek siteler '),
(2129, 0, 'kidstimewasting ', 0, '.Include</etc/dansguardian/lists/blacklists/kidstimewasting/domains>', '.Include</etc/dansguardian/lists/blacklists/kidstimewasting/urls>', 'kidstimewasting', 'Çocuklar için zaman kaybettirecek siteler '),
(2130, 0, 'posta ', 0, '.Include</etc/dansguardian/lists/blacklists/mail/domains>', '.Include</etc/dansguardian/lists/blacklists/mail/urls>', 'mail', 'Webmail ve e-posta siteleri '),
(2131, 0, 'marketingware ', 0, '.Include</etc/dansguardian/lists/blacklists/marketingware/domains>', '.Include</etc/dansguardian/lists/blacklists/marketingware/urls>', 'marketingware', 'Pazarlama ürünleri hakkında siteler '),
(2132, 0, 'tıbbi ', 0, '.Include</etc/dansguardian/lists/blacklists/medical/domains>', '.Include</etc/dansguardian/lists/blacklists/medical/urls>', 'medical', 'Tıp web siteleri '),
(2133, 1, 'mixed_adult ', 0, '.Include</etc/dansguardian/lists/blacklists/mixed_adult/domains>', '.Include</etc/dansguardian/lists/blacklists/mixed_adult/urls>', 'mixed_adult', 'Karışık yetişkin içerikli siteler '),
(2134, 0, 'naturism ', 0, '.Include</etc/dansguardian/lists/blacklists/naturism/domains>', '.Include</etc/dansguardian/lists/blacklists/naturism/urls>', 'naturism', 'Çıplak resim içeren ve / veya çıplak yaşam tarzını teşvik eden siteler '),
(2135, 0, 'haber ', 0, '.Include</etc/dansguardian/lists/blacklists/news/domains>', '.Include</etc/dansguardian/lists/blacklists/news/urls>', 'news', 'Haber siteleri '),
(2136, 0, 'onlineauctions ', 0, '.Include</etc/dansguardian/lists/blacklists/onlineauctions/domains>', '.Include</etc/dansguardian/lists/blacklists/onlineauctions/urls>', 'onlineauctions', 'Online açık artırmalar '),
(2137, 0, 'onlinegames ', 0, '.Include</etc/dansguardian/lists/blacklists/onlinegames/domains>', '.Include</etc/dansguardian/lists/blacklists/onlinegames/urls>', 'onlinegames', 'Online oyun siteleri '),
(2138, 0, 'onlinepayment ', 0, '.Include</etc/dansguardian/lists/blacklists/onlinepayment/domains>', '.Include</etc/dansguardian/lists/blacklists/onlinepayment/urls>', 'onlinepayment', 'Online ödeme siteleri '),
(2139, 0, 'personalfinance ', 0, '.Include</etc/dansguardian/lists/blacklists/personalfinance/domains>', '.Include</etc/dansguardian/lists/blacklists/personalfinance/urls>', 'personalfinance', 'Kişisel finans siteleri '),
(2140, 0, 'hayvanlar ', 0, '.Include</etc/dansguardian/lists/blacklists/pets/domains>', '.Include</etc/dansguardian/lists/blacklists/pets/urls>', 'pets', 'Hayvanlarla ilgili siteler'),
(2141, 0, 'phishing ', 0, '.Include</etc/dansguardian/lists/blacklists/phishing/domains>', '.Include</etc/dansguardian/lists/blacklists/phishing/urls>', 'phishing', 'Zarar vermek için avlama yöntemini kullanan siteler'),
(2142, 1, 'porno ', 1, '.Include</etc/dansguardian/lists/blacklists/porn/domains>', '.Include</etc/dansguardian/lists/blacklists/porn/urls>', 'porn', 'Pornografi '),
(2143, 0, 'proxy ', 0, '.Include</etc/dansguardian/lists/blacklists/proxy/domains>', '.Include</etc/dansguardian/lists/blacklists/proxy/urls>', 'proxy', 'Filtreleri atlamak için proxy siteleri'),
(2144, 0, 'radyo ', 0, '.Include</etc/dansguardian/lists/blacklists/radio/domains>', '.Include</etc/dansguardian/lists/blacklists/radio/urls>', 'radio', 'Radyo ve televizyon '),
(2145, 0, 'din ', 0, '.Include</etc/dansguardian/lists/blacklists/religion/domains>', '.Include</etc/dansguardian/lists/blacklists/religion/urls>', 'religion', 'Din ile ilgili siteler'),
(2146, 0, 'zil sesleri ', 0, '.Include</etc/dansguardian/lists/blacklists/ringtones/domains>', '.Include</etc/dansguardian/lists/blacklists/ringtones/urls>', 'ringtones', 'Zil sesleri, oyunlar, resimler ve diğer konularda siteler'),
(2147, 0, 'arama motorları ', 0, '.Include</etc/dansguardian/lists/blacklists/searchengines/domains>', '.Include</etc/dansguardian/lists/blacklists/searchengines/urls>', 'searchengines', 'Google gibi arama motorları '),
(2148, 0, 'mezhep ', 0, '.Include</etc/dansguardian/lists/blacklists/sect/domains>', '.Include</etc/dansguardian/lists/blacklists/sect/urls>', 'sect', 'Mezhep grupları hakkında siteler '),
(2149, 0, 'cinsellik ', 0, '.Include</etc/dansguardian/lists/blacklists/sexuality/domains>', '.Include</etc/dansguardian/lists/blacklists/sexuality/urls>', 'sexuality', 'Cinsellikle ilgili siteler eğitim materyali hariç'),
(2150, 0, 'sexualityeducation ', 0, '.Include</etc/dansguardian/lists/blacklists/sexualityeducation/domains>', '.Include</etc/dansguardian/lists/blacklists/sexualityeducation/urls>', 'sexualityeducation', 'Cinsellik hakkında eğitim bilgilerine ilişkin siteler. '),
(2151, 1, 'alışveriş ', 0, '.Include</etc/dansguardian/lists/blacklists/shopping/domains>', '.Include</etc/dansguardian/lists/blacklists/shopping/urls>', 'shopping', 'Alışveriş siteleri '),
(2152, 0, 'socialnetworking ', 0, '.Include</etc/dansguardian/lists/blacklists/socialnetworking/domains>', '.Include</etc/dansguardian/lists/blacklists/socialnetworking/urls>', 'socialnetworking', 'Sosyal ağ siteleri '),
(2153, 0, 'sportnews ', 0, '.Include</etc/dansguardian/lists/blacklists/sportnews/domains>', '.Include</etc/dansguardian/lists/blacklists/sportnews/urls>', 'sportnews', 'Spor haber siteleri '),
(2154, 0, 'spor ', 1, '.Include</etc/dansguardian/lists/blacklists/sports/domains>', '.Include</etc/dansguardian/lists/blacklists/sports/urls>', 'sports', 'Tüm spor siteleri '),
(2155, 0, 'casus ', 0, '.Include</etc/dansguardian/lists/blacklists/spyware/domains>', '.Include</etc/dansguardian/lists/blacklists/spyware/urls>', 'spyware', 'spyware indirme siteleri (Zararlı) '),
(2156, 0, 'updatesites ', 0, '.Include</etc/dansguardian/lists/blacklists/updatesites/domains>', '.Include</etc/dansguardian/lists/blacklists/updatesites/urls>', 'updatesites', 'Yazılım güncellemeleri, virüs sigs dahil indirilen siteler '),
(2157, 0, 'tatil ', 0, '.Include</etc/dansguardian/lists/blacklists/vacation/domains>', '.Include</etc/dansguardian/lists/blacklists/vacation/urls>', 'vacation', 'Tatil ile ilgili siteler '),
(2158, 0, 'şiddet ', 0, '.Include</etc/dansguardian/lists/blacklists/violence/domains>', '.Include</etc/dansguardian/lists/blacklists/violence/urls>', 'violence', 'Şiddet içeren siteler '),
(2159, 0, 'virusinfected ', 0, '.Include</etc/dansguardian/lists/blacklists/virusinfected/domains>', '.Include</etc/dansguardian/lists/blacklists/virusinfected/urls>', 'virusinfected', 'Virus bulaşmış siteler'),
(2160, 1, 'warez ', 0, '.Include</etc/dansguardian/lists/blacklists/warez/domains>', '.Include</etc/dansguardian/lists/blacklists/warez/urls>', 'warez', 'Yasadışı korsan yazılım ile ilgili siteler '),
(2161, 0, 'hava ', 0, '.Include</etc/dansguardian/lists/blacklists/weather/domains>', '.Include</etc/dansguardian/lists/blacklists/weather/urls>', 'weather', 'Hava durumu siteleri'),
(2162, 0, 'silahlar ', 0, '.Include</etc/dansguardian/lists/blacklists/weapons/domains>', '.Include</etc/dansguardian/lists/blacklists/weapons/urls>', 'weapons', 'Silahla ilgili/satan siteler '),
(2163, 0, 'webmail ', 0, '.Include</etc/dansguardian/lists/blacklists/webmail/domains>', '.Include</etc/dansguardian/lists/blacklists/webmail/urls>', 'webmail', 'Sadece web-posta siteleri '),
(2164, 0, 'beyaz listesi ', 0, '.Include</etc/dansguardian/lists/blacklists/whitelist/domains>', '.Include</etc/dansguardian/lists/blacklists/whitelist/urls>', 'whitelist', 'Çocuklar için uygun içeriği olan siteler');

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `filtre_kategori_secim`
--

CREATE TABLE IF NOT EXISTS `filtre_kategori_secim` (
  `kategori` int(11) NOT NULL,
  `izin_yasak` int(11) NOT NULL,
  `grup` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Tablo döküm verisi `filtre_kategori_secim`
--

INSERT INTO `filtre_kategori_secim` (`kategori`, `izin_yasak`, `grup`) VALUES
(2088, 0, 2),
(2137, 2, 3);

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `filtre_kullanicilar`
--

CREATE TABLE IF NOT EXISTS `filtre_kullanicilar` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tur` tinyint(4) NOT NULL COMMENT '0 host, 1 group',
  `kullanici` int(11) NOT NULL,
  `filtre_grubu` int(11) NOT NULL,
  `update_yok` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `groupitem`
--

CREATE TABLE IF NOT EXISTS `groupitem` (
  `group` int(11) NOT NULL,
  `host` int(11) NOT NULL,
  PRIMARY KEY (`group`,`host`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Tablo döküm verisi `groupitem`
--

INSERT INTO `groupitem` (`group`, `host`) VALUES
(1, 7),
(1, 8),
(1, 9),
(1, 10),
(1, 11),
(1, 12),
(1, 13),
(1, 14),
(1, 15),
(2, 4),
(2, 16),
(2, 17),
(2, 18),
(2, 19),
(2, 20),
(2, 21),
(2, 22),
(2, 23),
(2, 24),
(2, 25),
(2, 64);

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `host`
--

CREATE TABLE IF NOT EXISTS `host` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `merkezId` int(11) DEFAULT NULL,
  `tur` smallint(6) NOT NULL,
  `name` varchar(40) NOT NULL,
  `address` varchar(15) NOT NULL,
  `netmask` varchar(15) NOT NULL,
  `comment` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='tur = 0 system, 1 center, 2 local' AUTO_INCREMENT=70 ;

--
-- Tablo döküm verisi `host`
--

INSERT INTO `host` (`id`, `merkezId`, `tur`, `name`, `address`, `netmask`, `comment`) VALUES
(1, NULL, 2, 'All', '0.0.0.0', '0.0.0.0', 'All'),
(4, NULL, 2, 'Atatürk Devlet', '212.175.140.8', '255.255.255.248', 'Atatürk devlet IP Bloğu'),
(5, NULL, 2, 'LAN', '192.168.0.0', '255.255.255.0', 'Lokal ağ'),
(7, NULL, 2, 'facebook-1', '173.252.64.0', '255.255.192.0', 'facebook-1'),
(8, NULL, 2, 'facebook-2', '31.13.24.0', '255.255.248.0', 'facebook-2'),
(9, NULL, 2, 'facebook-3', '31.13.64.0', '255.255.192.0', 'facebook-3'),
(10, NULL, 2, 'facebook-4', '66.220.144.0', '255.255.240.0', 'facebook-4'),
(11, NULL, 2, 'facebook-5', '69.63.176.0', '255.255.240.0', 'facebook-5'),
(12, NULL, 2, 'facebook-6', '69.171.224.0', '255.255.224.0', 'facebook-6'),
(13, NULL, 2, 'facebook-7', '74.119.76.0', '255.255.252.0', 'facebook-7'),
(14, NULL, 2, 'facebook-8', '103.4.96.0', '255.255.252.0', 'facebook-8'),
(15, NULL, 2, 'facebook-9', '204.15.20.0', '255.255.252.0', 'facebook-9'),
(16, NULL, 2, 'Halk Sağlığı', '212.156.113.146', '255.255.255.255', 'Halk Sağlığı'),
(17, NULL, 2, 'TIG-1', '195.175.75.14', '255.255.255.255', 'TIG-1'),
(18, NULL, 2, 'TIG-2', '195.175.171.2', '255.255.255.255', 'TIG-2'),
(19, NULL, 2, 'Sağlık Hiz.Gen.Md.', '95.0.0.69', '255.255.255.255', 'Sağlık Hiz.Gen.Md.'),
(20, NULL, 2, 'Kardelen Yazılım', '85.11.24.19', '255.255.255.255', 'Kardelen Yazılım'),
(21, NULL, 2, 'SGK', '195.245.227.0', '255.255.255.0', 'SGK'),
(22, NULL, 2, 'saglik.gov.tr', '212.175.169.0', '255.255.255.0', 'saglik.gov.tr'),
(23, NULL, 2, 'kardelen yazılım depo', '46.45.136.115', '255.255.255.255', 'kardelen yazılım depo'),
(24, NULL, 2, 'kardelen yazılım depo-2', '46.45.136.126', '255.255.255.255', 'kardelen yazılım depo-2'),
(25, NULL, 2, 'abbott.com', '130.36.0.0', '255.255.0.0', 'Aboot Lab. Cihazının kalibrasyonu için'),
(60, NULL, 2, 'Çaycuma Dev.Hast.', '212.156.113.162', '255.255.255.255', 'Çaycuma Dev.Hast.'),
(63, NULL, 2, 'kesenek.gov.tr ip blogu', '195.245.227.0', '255.255.255.0', 'kesenek.gov.tr ip blogu'),
(64, NULL, 2, ' analitikbutce.sgbsaglik.gov.tr ip blogu', '95.0.140.224', '255.255.255.240', ' analitikbutce.sgbsaglik.gov.tr ip blogu'),
(65, NULL, 2, 'Recep Server', '192.168.0.101', '255.255.255.255', 'Recep Server'),
(66, NULL, 2, 'Web Servisler Karmed 2', '192.168.0.155', '255.255.255.255', 'Web Servisler Karmed IP-0.155'),
(67, NULL, 2, 'WAN IP', '88.248.255.146', '255.255.255.255', 'WAN IP'),
(68, NULL, 2, 'Web Servisler Karmed 1', '192.168.0.100', '255.255.255.255', 'Web Servisler Karmed IP-0.100'),
(69, NULL, 2, 'Atatürk Devlet Hastanesi WAN IP', '212.175.140.10', '255.255.255.255', 'Atatürk Devlet Hastanesi WAN IP');

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `hostgroup`
--

CREATE TABLE IF NOT EXISTS `hostgroup` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tur` int(11) NOT NULL DEFAULT '2',
  `name` varchar(40) NOT NULL,
  `comment` varchar(150) DEFAULT NULL,
  `merkezId` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Tablo döküm verisi `hostgroup`
--

INSERT INTO `hostgroup` (`id`, `tur`, `name`, `comment`, `merkezId`) VALUES
(1, 2, 'Facebook IP Block', 'Facebook IP Block', NULL),
(2, 2, 'Direk Erişim', 'İç Network(LAN) to Direk Erişim(WAN)', NULL);

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `inputfirewall`
--

CREATE TABLE IF NOT EXISTS `inputfirewall` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `if` varchar(40) NOT NULL,
  `sGroup` tinyint(4) NOT NULL,
  `sName` int(11) NOT NULL,
  `service` varchar(40) NOT NULL,
  `policy` varchar(6) CHARACTER SET big5 NOT NULL,
  `comment` varchar(150) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Tablo döküm verisi `inputfirewall`
--

INSERT INTO `inputfirewall` (`id`, `if`, `sGroup`, `sName`, `service`, `policy`, `comment`) VALUES
(2, 'Any', 0, 5, '32', 'ACCEPT', 'ARGUSGuard Web Erişim'),
(3, 'Any', 0, 1, '8', 'ACCEPT', 'ARGUSGuard Default Kural ---- Sisteminize göre düzenleyiniz ---- '),
(4, 'Any', 0, 1, '7', 'ACCEPT', 'ARGUSGuard Default Kural ---- Sisteminize göre düzenleyiniz ----'),
(5, 'eth1', 0, 5, '10', 'ACCEPT', 'Ağ trafiği izleme'),
(6, 'eth1', 0, 5, '33', 'ACCEPT', 'Argus SSH Erişim');

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `interfaces`
--

CREATE TABLE IF NOT EXISTS `interfaces` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `if` varchar(10) NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  `type` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_UNIQUE` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Tablo döküm verisi `interfaces`
--

INSERT INTO `interfaces` (`id`, `if`, `name`, `type`) VALUES
(1, 'Any', NULL, NULL),
(2, 'eth0', 'Dış Bacak', 'External'),
(3, 'eth1', 'İç Bacak', 'Internal');

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `internettolan`
--

CREATE TABLE IF NOT EXISTS `internettolan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `if` varchar(40) NOT NULL,
  `sGroup` tinyint(4) NOT NULL,
  `sName` int(11) NOT NULL,
  `dName` int(11) NOT NULL,
  `service` varchar(40) NOT NULL,
  `redirectHost` varchar(40) NOT NULL,
  `redirectService` varchar(40) NOT NULL,
  `policy` varchar(6) NOT NULL,
  `comment` varchar(150) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `kullanicilar`
--

CREATE TABLE IF NOT EXISTS `kullanicilar` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(30) NOT NULL,
  `password` varchar(50) NOT NULL,
  `full_name` varchar(30) NOT NULL,
  `merkez_kullanicisi` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_name` (`user_name`),
  KEY `id` (`id`),
  KEY `user_name_2` (`user_name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Tablo döküm verisi `kullanicilar`
--

INSERT INTO `kullanicilar` (`id`, `user_name`, `password`, `full_name`, `merkez_kullanicisi`) VALUES
(1, 'Admin', 'Argus', 'Argus Guard Sistem Yöneticisi', 1);

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `lantointernet`
--

CREATE TABLE IF NOT EXISTS `lantointernet` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `if` varchar(40) NOT NULL,
  `sGroup` tinyint(4) NOT NULL,
  `sName` int(11) NOT NULL,
  `dGroup` tinyint(4) NOT NULL,
  `dName` int(11) NOT NULL,
  `service` int(11) NOT NULL,
  `policy` varchar(6) NOT NULL,
  `comment` varchar(150) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=15 ;

--
-- Tablo döküm verisi `lantointernet`
--

INSERT INTO `lantointernet` (`id`, `if`, `sGroup`, `sName`, `dGroup`, `dName`, `service`, `policy`, `comment`) VALUES
(3, 'eth1', 0, 5, 0, 1, 18, 'ACCEPT', 'dns'),
(4, 'eth1', 0, 5, 0, 1, 17, 'ACCEPT', 'dns'),
(9, 'eth1', 0, 5, 1, 2, 1, 'ACCEPT', 'Full izin'),
(13, 'eth1', 0, 5, 1, 1, 1, 'DROP', 'Facebook block'),
(14, 'eth1', 0, 5, 0, 1, 9, 'ACCEPT', 'https izin');

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `service`
--

CREATE TABLE IF NOT EXISTS `service` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tur` smallint(6) NOT NULL,
  `name` varchar(40) NOT NULL,
  `port` int(11) NOT NULL,
  `protokol` varchar(5) NOT NULL,
  `comment` varchar(150) DEFAULT NULL,
  `merkezId` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=34 ;

--
-- Tablo döküm verisi `service`
--

INSERT INTO `service` (`id`, `tur`, `name`, `port`, `protokol`, `comment`, `merkezId`) VALUES
(1, 2, 'Any', 0, '', 'Any', NULL),
(5, 2, 'ssh', 22, 'tcp', 'ssh', NULL),
(6, 2, 'http', 80, 'tcp', 'http', NULL),
(7, 2, 'monitorix', 9999, 'tcp', 'monitorix', NULL),
(8, 2, 'http-alt', 8080, 'tcp', 'http-alt', NULL),
(9, 2, 'https', 443, 'tcp', 'https', NULL),
(10, 2, 'ntop', 3000, 'tcp', 'ntop', NULL),
(11, 2, 'ftp', 21, 'tcp', 'ftp', NULL),
(12, 2, 'ftp', 20, 'tcp', 'ftp', NULL),
(13, 2, 'ftp', 20, 'udp', 'ftp', NULL),
(14, 2, 'ftp', 21, 'udp', 'ftp', NULL),
(15, 2, 'telnet', 23, 'tcp', 'telnet', NULL),
(16, 2, 'smtp', 25, 'tcp', 'smtp', NULL),
(17, 2, 'dns', 53, 'tcp', 'dns', NULL),
(18, 2, 'dns', 53, 'udp', 'dns', NULL),
(19, 2, 'pop3', 110, 'tcp', 'pop3', NULL),
(20, 2, 'ntp', 123, 'udp', 'NTP (Network Time Protocol)', NULL),
(21, 2, 'smtp-ssl', 465, 'tcp', 'SMTP  SSL', NULL),
(22, 2, 'syslog', 514, 'udp', 'syslog ', NULL),
(23, 2, 'imap-ssl', 993, 'tcp', 'SSL üzerinden IMAP4 (Kriptolanmış aktarım)', NULL),
(24, 2, 'pop3-ssl', 995, 'tcp', 'SSL üzerinden POP3 Protokolü (Kriptolanmış aktarım)', NULL),
(25, 2, 'oracle', 1521, 'tcp', 'oracle', NULL),
(26, 2, 'Web Servisler Karmed Port Kapısı 1', 8200, 'tcp', 'Web Servisler Karmed Port Kapısı 8200', NULL),
(27, 2, 'Karar Destek Port', 41286, 'tcp', 'Karar Destek Port', NULL),
(28, 2, 'Karar Destek Port Kapısı', 1522, 'tcp', 'Karar Destek Port Kapısı 1522', NULL),
(29, 2, 'GlassFish Port Kapısı 1', 4848, 'tcp', 'GlassFish Port Kapısı 4848', NULL),
(30, 2, 'GlassFish Port Kapısı 2', 8085, 'tcp', 'GlassFish Port Kapısı 8085', NULL),
(31, 2, 'Aile Hekimleri Laboratuvar Port SSH', 10022, 'tcp', 'Aile Hekimleri Laboratuvar Port Kapısı SSH-10022', NULL),
(32, 2, 'Argus Web Erişim', 26767, 'tcp', 'Argus Web Erişim', NULL),
(33, 2, 'Argus SSH Erişim', 26868, 'tcp', 'Argus SSH Erişim', NULL);

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `uzak_baglanti`
--

CREATE TABLE IF NOT EXISTS `uzak_baglanti` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kurum_adi` varchar(60) NOT NULL,
  `database` varchar(100) NOT NULL,
  `user_name` varchar(45) NOT NULL,
  `password` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Tablo döküm verisi `uzak_baglanti`
--

INSERT INTO `uzak_baglanti` (`id`, `kurum_adi`, `database`, `user_name`, `password`) VALUES
(1, 'Deneme Kurumu Merkez', 'mysql:host=localhost;dbname=4UsAdmin', 'root', ''),
(2, 'Burası Merkez Değil', 'mysql:host=localhost;dbname=4UsAdmin.', 'root', '');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;