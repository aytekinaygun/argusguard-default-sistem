#!/bin/bash

apt-get -y install dansguardian
apt-get -y install apache2 php5 libapache2-mod-php5 php5-gd mysql-server mysql-client php5-mysql phpmyadmin
apt-get -y install ntpdate
apt-get -y install arpwatch
apt-get -y install swatch
apt-get -y install sudo
apt-get -y install ethtool
apt-get -y install python-mysqldb python-netifaces python-configobj python-beautifulsoup
apt-get -y install curl
apt-get -y install postfix
apt-get -y install rsync
apt-get -y install ntop
apt-get -y install tcpdump
apt-get -y install git
apt-get -y install sarg
apt-get -y install speedometer

# backports deposu eklenir.
echo "deb http://ftp.debian.org/debian wheezy-backports main contrib non-free" >> /etc/apt/sources.list
apt-get update
#echo "deb-src http://ftp.debian.org/debian wheezy-backports main contrib non-free" >> /etc/apt/sources.list

# squid3 https destegi
apt-get -y -t wheezy-backports install squid3
dpkg -i squid3_3.4.8-5~bpo70+1_amd64.deb squid3-common_3.4.8-5~bpo70+1_all.deb
squid3 -v |grep ssl --colour

# monitorix
echo "deb http://apt.izzysoft.de/ubuntu generic universe" >> /etc/apt/sources.list
wget http://apt.izzysoft.de/izzysoft.asc
apt-key add izzysoft.asc
apt-get update
apt-get -y install monitorix
cp monitorix.conf /etc/monitorix/monitorix.conf
chown root: /etc/monitorix/monitorix.conf

# ssh port
sed -i s/"Port 22"/"Port 26868"/g /etc/ssh/sshd_config

# apache port
cp ports.conf /etc/apache2/ports.conf
cp 000-default /etc/apache2/sites-enabled/000-default.conf
chown root: -R /etc/apache2

# zaman sunucusu
ntpdate pool.ntp.org
cp ntpdate /etc/cron.daily/
chown root: -R /etc/cron.daily/ntpdate
chmod +x /etc/cron.daily/ntpdate

# squid3
cp squid.conf /etc/squid3/squid.conf
chown root: /etc/squid3/squid.conf

# scriptler
cp scripts/* /usr/local/sbin/
chown root: -R /usr/local/sbin/
chmod +x /usr/local/sbin/*

# fw local ve center dosyalari
mkdir /usr/local/sbin/fw
touch /usr/local/sbin/fw/fw-center-rules
touch /usr/local/sbin/fw/fw-local-rules

# argus.conf
mkdir /etc/argusguard
cp argusguard.conf /etc/argusguard/
chown root: -R /etc/argusguard

# dansguardian
cp -r dansguardian /etc/argusguard/
chown www-data: -R /etc/argusguard/dansguardian

# backup
mkdir /var/backup
mkdir /var/backup/fw-backup
chown -R www-data: /var/backup/fw-backup

# sudoers
cp sudoers /etc/sudoers
chown root: /etc/sudoers

# rc.local
cp rc.local /etc/rc.local
chmod +x /etc/rc.local
chown root: /etc/rc.local

# rsyslog.conf: iptables logları syslog ve kern.log'dan ayrılır.
mkdir /var/log/argusguard
cp rsyslog.conf /etc/
/etc/init.d/rsyslog restart

# firewall logrotate
cp argusguard-firewall /etc/logrotate.d/

# dansguardian logrotate
mkdir /var/log/dansguardian/archive
chown dansguardian: /var/log/dansguardian/archive/
rm /etc/logrotate.d/dansguardi*
cp logrotate.dansguardian /etc/argusguard/
cp cron-dansguardian /etc/cron.d/

# sarg
cp sarg.conf /etc/sarg/sarg.conf
cp sarg-reports.conf /etc/sarg/sarg-reports.conf
cp sarg-reports-today /etc/cron.d/sarg-reports-today
cp sarg-reports-daily /etc/cron.d/sarg-reports-daily

# squid https icin sertifika olustur
mkdir /etc/squid3/ssl_cert
chmod 700 /etc/squid3/ssl_cert
openssl req -new -newkey rsa:2048 -sha256 -days 3650 -nodes -x509 -keyout argusguardCA.pem  -out argusguardCA.pem
### Sorulara cevap verilir...
#Country Name (2 letter code) [AU]:TR
#State or Province Name (full name) [Some-State]:TR
#Locality Name (eg, city) []:
#Organization Name (eg, company) [Internet Widgits Pty Ltd]:ArgusGuard
#Organizational Unit Name (eg, section) []:ArgusGuard
#Common Name (e.g. server FQDN or YOUR name) []:
#Email Address []:
###
openssl x509 -in argusguardCA.pem -outform DER -out argusguardCA.der
mv argusguardCA.der /etc/squid3/ssl_cert
mv argusguardCA.pem /etc/squid3/ssl_cert
/usr/lib/squid3/ssl_crtd -c -s /var/lib/ssl_db
chown -R proxy: /etc/squid3/ssl_cert
chown -R proxy: /var/lib/ssl_db
mkdir /etc/squid3/rules

# servisler restart
/etc/init.d/dansguardian restart
/etc/init.d/squid3 restart
/etc/init.d/apache2 restart
/etc/init.d/monitorix restart
/etc/init.d/ssh restart